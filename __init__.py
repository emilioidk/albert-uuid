# -*- coding: utf-8 -*-

"""Copy a random UUID v4 to the clipboard.

Trigger: uuid"""

from albert import *
import os
import uuid

__title__ = "UUID"
__version__ = "0.4.2"
__triggers__ = "uuid"
__authors__ = "Emilio Martin Lundgaard Lopez"

iconPath = os.path.dirname(__file__)+"/icon.png"


def handleQuery(query):
    if not query.isTriggered:
        return

    if query.rawString.startswith('uuid'):
        random_uuid = str(uuid.uuid4())

        return [Item(
            id='uuid',
            icon=iconPath,
            text=random_uuid,
            subtext="Random UUID",
            actions=[
                ClipAction(text="Copy to clipboard",
                           clipboardText=random_uuid)
            ]
        )]
